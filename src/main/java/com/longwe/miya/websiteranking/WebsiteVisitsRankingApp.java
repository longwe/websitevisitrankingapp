package com.longwe.miya.websiteranking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.longwe.miya.websiteranking.configuration.JpaConfiguration;

/**
 * Miya W. Longwe
 * The Class WebsiteVisitsRankingApp.
 */
@Import(JpaConfiguration.class)
@SpringBootApplication(scanBasePackages={"com.longwe.miya.websiteranking"})// same as @Configuration @EnableAutoConfiguration @ComponentScan
public class WebsiteVisitsRankingApp {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(WebsiteVisitsRankingApp.class, args);
	}
}
