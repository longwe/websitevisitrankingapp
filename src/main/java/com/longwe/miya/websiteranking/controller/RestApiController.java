package com.longwe.miya.websiteranking.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.longwe.miya.websiteranking.model.WebsiteVisit;
import com.longwe.miya.websiteranking.service.WebsiteVisitService;
import com.longwe.miya.websiteranking.util.CustomErrorType;


/**
 * Miya W. Longwe
 * The Class RestApiController.
 */
@RestController
@RequestMapping("/api")
public class RestApiController {

	/** The Constant logger. */
	public static final Logger logger = LoggerFactory.getLogger(RestApiController.class);
	
	/** The Constant TOP_MOST_SEARCHED_WEBSITE. */
	public static final int TOP_MOST_SEARCHED_WEBSITE = 1;
	
	/** The Constant LEAST_SEARCHED_WEBSITE. */
	public static final int LEAST_SEARCHED_WEBSITE = 2;

	/** The website visit service. */
	@Autowired
	WebsiteVisitService websiteVisitService; // Service which will do all data
												// retrieval/manipulation work

	// -------------------Retrieve All
	// WebsiteVisits---------------------------------------------

	/**
												 * List all website visits.
												 *
												 * @return the response entity
												 */
												@RequestMapping(value = "/websitevisit/", method = RequestMethod.GET)
	public ResponseEntity<List<WebsiteVisit>> listAllWebsiteVisits() {
		List<WebsiteVisit> websiteVisits = websiteVisitService.findAllWebsiteVisits();

		if (websiteVisits.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// We many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<WebsiteVisit>>(websiteVisits, HttpStatus.OK);
	}

	// -------------------Retrieve Single
	// ----WebsiteVisit--------------------------------------

	/**
	 * Gets the website visit.
	 *
	 * @param id the id
	 * @return the website visit
	 */
	@RequestMapping(value = "/websitevisit/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getWebsiteVisit(@PathVariable("id") long id) {
		logger.info("Fetching WebsiteVisit with id {}", id);
		WebsiteVisit websiteVisit = websiteVisitService.findById(id);
		if (websiteVisit == null) {
			logger.error("WebsiteVisit with id {} not found.", id);
			return new ResponseEntity(new CustomErrorType("WebsiteVisit with id " + id + " not found"),
					HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<WebsiteVisit>(websiteVisit, HttpStatus.OK);
	}

	// -------------------Create a
	// WebsiteVisit-------------------------------------------

	/**
	 * Creates the website visit.
	 *
	 * @param websiteVisit the website visit
	 * @param ucBuilder the uc builder
	 * @return the response entity
	 */
	@RequestMapping(value = "/websitevisit/", method = RequestMethod.POST)
	public ResponseEntity<?> createWebsiteVisit(@RequestBody WebsiteVisit websiteVisit,
			UriComponentsBuilder ucBuilder) {
		logger.info("Creating WebsiteVisit : {}", websiteVisit);

		if (websiteVisitService.isWebsiteVisitExist(websiteVisit)) {
			logger.error("Unable to create. A WebsiteVisit with name {} already exist", websiteVisit.getSiteUrl());
			return new ResponseEntity(new CustomErrorType(
					"Unable to create. A WebsiteVisit with name " + websiteVisit.getSiteUrl() + " already exist."),
					HttpStatus.CONFLICT);
		}
		websiteVisitService.saveWebsiteVisit(websiteVisit);

		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/api/websiteVisit/{id}").buildAndExpand(websiteVisit.getId()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// ------------------- Update a WebsiteVisit
	// ------------------------------------------------

	/**
	 * Update website visit.
	 *
	 * @param id the id
	 * @param websiteVisit the website visit
	 * @return the response entity
	 */
	@RequestMapping(value = "/websiteVisit/{id}", method = RequestMethod.PUT)
	public ResponseEntity<?> updateWebsiteVisit(@PathVariable("id") long id, @RequestBody WebsiteVisit websiteVisit) {
		logger.info("Updating WebsiteVisit with id {}", id);

		WebsiteVisit currentWebsiteVisit = websiteVisitService.findById(id);

		if (currentWebsiteVisit == null) {
			logger.error("Unable to update. WebsiteVisit with id {} not found.", id);
			return new ResponseEntity(
					new CustomErrorType("Unable to upate. WebsiteVisit with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}

		currentWebsiteVisit.setSiteUrl(websiteVisit.getSiteUrl());
		currentWebsiteVisit.setVisitNumber(websiteVisit.getVisitNumber());
		currentWebsiteVisit.setVisitDate(websiteVisit.getVisitDate());

		websiteVisitService.updateWebsiteVisit(currentWebsiteVisit);
		return new ResponseEntity<WebsiteVisit>(currentWebsiteVisit, HttpStatus.OK);
	}

	// ------------------- Delete a
	// WebsiteVisit-----------------------------------------

	/**
	 * Delete website visit.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@RequestMapping(value = "/websiteVisit/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteWebsiteVisit(@PathVariable("id") long id) {
		logger.info("Fetching & Deleting WebsiteVisit with id {}", id);

		WebsiteVisit websiteVisit = websiteVisitService.findById(id);
		if (websiteVisit == null) {
			logger.error("Unable to delete. WebsiteVisit with id {} not found.", id);
			return new ResponseEntity(
					new CustomErrorType("Unable to delete. WebsiteVisit with id " + id + " not found."),
					HttpStatus.NOT_FOUND);
		}
		websiteVisitService.deleteWebsiteVisitById(id);
		return new ResponseEntity<WebsiteVisit>(HttpStatus.NO_CONTENT);
	}

	// ------------------- Delete All WebsiteVisits-----------------------------

	/**
	 * Delete all website visits.
	 *
	 * @return the response entity
	 */
	@RequestMapping(value = "/websiteVisit/", method = RequestMethod.DELETE)
	public ResponseEntity<WebsiteVisit> deleteAllWebsiteVisits() {
		logger.info("Deleting All WebsiteVisits");

		websiteVisitService.deleteAllWebsiteVisit();
		return new ResponseEntity<WebsiteVisit>(HttpStatus.NO_CONTENT);
	}

	//// ------------------- Get Distinct Site Visit
	//// Dates-----------------------------

	/**
	 * List all visit dates.
	 *
	 * @return the response entity
	 */
	@RequestMapping(value = "/websiteVisitDates/", method = RequestMethod.GET)
	public ResponseEntity<List<String>> listAllVisitDates() {

		Set<LocalDate> visitDates = websiteVisitService.findAllVisitDates();

		if (visitDates.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<String>>(convertLocalDateToString(visitDates), HttpStatus.OK);
	}

	/**
	 * Convert local date to string.
	 *
	 * @param visitDates the visit dates
	 * @return the list
	 */
	private List<String> convertLocalDateToString(Set<LocalDate> visitDates) {

		List<String> visitedDates = new ArrayList<String>();

		if (!visitDates.isEmpty()) {
			for (Iterator iterator = visitDates.iterator(); iterator.hasNext();) {
				LocalDate localDate = (LocalDate) iterator.next();

				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

				String formatDateTime = localDate.format(formatter);
				visitedDates.add(formatDateTime);

			}
		}

		Collections.sort(visitedDates);
		return visitedDates;

	}

	/**
	 * Gets the top five websites.
	 *
	 * @param visitDate the visit date
	 * @return the top five websites
	 */
	@RequestMapping(value = "/site/{visitDate}", method = RequestMethod.GET)
	public ResponseEntity<List<WebsiteVisit>> getTopFiveWebsites(@PathVariable("visitDate") String visitDate) {
		logger.info("Fetching records with visit date {}", visitDate);
		List<WebsiteVisit> topFiveSites = new ArrayList<WebsiteVisit>();

		if (visitDate != null) {
			topFiveSites = websiteVisitService.getTopFiveVisitedSitesByDate(LocalDate.parse(visitDate));

			if (topFiveSites == null) {
				logger.error("No records found with visit date ", visitDate);
				return new ResponseEntity(new CustomErrorType("Website visits with date " + visitDate + " not found"),
						HttpStatus.NOT_FOUND);
			}

		}
		return new ResponseEntity<List<WebsiteVisit>>(getTopFiveSites(topFiveSites), HttpStatus.OK);
	}

	/**
	 * Gets the top five sites.
	 *
	 * @param allsites the allsites
	 * @return the top five sites
	 */
	private List<WebsiteVisit> getTopFiveSites(List<WebsiteVisit> allsites) {

		if (!allsites.isEmpty()) {

			Collections.sort(allsites, WebsiteVisit.sortByNumberOfVisitsDesc);
			return getTopFiveList(allsites);
		}

		return allsites;

	}

	/**
	 * Gets the top five list.
	 *
	 * @param visitList the visit list
	 * @return the top five list
	 */
	private List<WebsiteVisit> getTopFiveList(List<WebsiteVisit> visitList) {

		List<WebsiteVisit> editeList = new ArrayList<WebsiteVisit>();
		int topFive = 4;
		int counter = 0;

		for (Iterator iterator = visitList.iterator(); iterator.hasNext();) {
			WebsiteVisit websiteVisit = (WebsiteVisit) iterator.next();
			if (counter > topFive) {
				break;
			}
			editeList.add(websiteVisit);

			counter++;

		}

		return editeList;

	}

	//// ------------------- Get Distinct Url Site
	//// Names-----------------------------

	/**
	 * List all url names.
	 *
	 * @return the response entity
	 */
	@RequestMapping(value = "/websiteUrlNames/", method = RequestMethod.GET)
	public ResponseEntity<List<String>> listAllUrlNames() {

		Set<String> siteUrlNames = websiteVisitService.findAllSiteUrlNames();

		if (siteUrlNames.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// You many decide to return HttpStatus.NOT_FOUND
		}
		return new ResponseEntity<List<String>>(siteUrlNames.stream().collect(Collectors.toList()), HttpStatus.OK);

	}
	//// ------------------- Get Distinct Url Site
	//// Names-----------------------------

	/**
	 * Gets the website by name.
	 *
	 * @param urlName the url name
	 * @return the website by name
	 */
	@RequestMapping(value = "/websiteUrlNames/{urlName}", method = RequestMethod.GET)
	public ResponseEntity<List<WebsiteVisit>> getWebsiteByName(@PathVariable("urlName") String urlName) {
		logger.info("Fetching records with urlName {}", urlName);
		List<WebsiteVisit> webisVisits = new ArrayList<WebsiteVisit>();

		if (urlName != null) {
			String urlNameParm = urlName.replace(".au", "");
			webisVisits = websiteVisitService.findWebsiteVisitBySiteUrlStartingWith(urlName.replace(".au", ""));

			if (webisVisits == null) {
				logger.error("No records found with visit date ", urlName);
				return new ResponseEntity(new CustomErrorType("Website visits with urlName " + urlName + " not found"),
						HttpStatus.NOT_FOUND);
			}

		}
		return new ResponseEntity<List<WebsiteVisit>>(webisVisits, HttpStatus.OK);
	}

	/**
	 * Gets the most or least website visited.
	 *
	 * @param searchQryId the search qry id
	 * @return the most or least website visited
	 */
	@RequestMapping(value = "/websitevisitrank/{searchQryId}", method = RequestMethod.GET)
	public ResponseEntity<List<WebsiteVisit>> getMostOrLeastWebsiteVisited(@PathVariable("searchQryId") Long searchQryId) {
		
		List<WebsiteVisit> websiteVisits = websiteVisitService.findAllWebsiteVisits();

		if (websiteVisits.isEmpty()) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
			// We many decide to return HttpStatus.NOT_FOUND
		}
		
		List<WebsiteVisit> siteVisitRank =getMostOrLeastVisitedSite(websiteVisits,searchQryId);
		return new ResponseEntity<List<WebsiteVisit>>(siteVisitRank, HttpStatus.OK);

	}
	
	/**
	 * Gets the most or least visited site.
	 *
	 * @param allVisits the all visits
	 * @param qryParm the qry parm
	 * @return the most or least visited site
	 */
	private List<WebsiteVisit> getMostOrLeastVisitedSite(List<WebsiteVisit> allVisits, Long qryParm){
		
		List<WebsiteVisit> sites = new ArrayList<WebsiteVisit>();
		if (!allVisits.isEmpty()){
			if (qryParm==TOP_MOST_SEARCHED_WEBSITE){
				Collections.sort(allVisits, WebsiteVisit.sortByNumberOfVisitsDesc);
				sites.add(allVisits.get(0));
			} else if (qryParm==LEAST_SEARCHED_WEBSITE){
			Collections.sort(allVisits, WebsiteVisit.sortByNumberOfVisitsAsc);
			sites.add(allVisits.get(0))	;
		} else {
			logger.error("No search criteria provided to retrieve top or least searched webiste");
			sites =allVisits;
		}
		}
		return sites;
		
	
	}
}