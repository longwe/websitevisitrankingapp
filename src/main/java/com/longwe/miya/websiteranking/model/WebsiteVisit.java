/**
 * 
 */
package com.longwe.miya.websiteranking.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.hql.internal.ast.util.NodeTraverser.VisitationStrategy;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * @author Miya W. Longwe
 *
*/ 
@Entity
@Table(name="app_website")
public class WebsiteVisit  implements Serializable{
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "visit_date", columnDefinition = "DATETIME", nullable = false)
	@JsonFormat(pattern = "YYYY-MM-dd")
	private LocalDate visitDate;

	
	@NotEmpty
	@Column(name="site_url", nullable=false)
	private String siteUrl;
	
	@NotEmpty
	@Column(name="visit_num", nullable=false)
	private Long visitNumber;
	
	private String convertedVisitDate;

	/**
	 * @return the convertedVisitDate
	 */
	public String getConvertedVisitDate() {
		
	 convertedVisitDate= this.getVisitDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	 return  convertedVisitDate;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the visitDate
	 */
	public LocalDate getVisitDate() {
		return visitDate;
	}

	/**
	 * @param visitDate the visitDate to set
	 */
	public void setVisitDate(LocalDate visitDate) {
		this.visitDate = visitDate;
	}

	/**
	 * @return the siteUrl
	 */
	public String getSiteUrl() {
		return siteUrl;
	}

	/**
	 * @param siteUrl the siteUrl to set
	 */
	public void setSiteUrl(String siteUrl) {
		this.siteUrl = siteUrl;
	}

	/**
	 * @return the visitNumber
	 */
	public Long getVisitNumber() {
		return visitNumber;
	}

	/**
	 * @param visitNumber the visitNumber to set
	 */
	public void setVisitNumber(Long visitNumber) {
		this.visitNumber = visitNumber;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((siteUrl == null) ? 0 : siteUrl.hashCode());
		result = prime * result + ((visitDate == null) ? 0 : visitDate.hashCode());
		result = prime * result + ((visitNumber == null) ? 0 : visitNumber.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WebsiteVisit other = (WebsiteVisit) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (siteUrl == null) {
			if (other.siteUrl != null)
				return false;
		} else if (!siteUrl.equals(other.siteUrl))
			return false;
		if (visitDate == null) {
			if (other.visitDate != null)
				return false;
		} else if (!visitDate.equals(other.visitDate))
			return false;
		if (visitNumber == null) {
			if (other.visitNumber != null)
				return false;
		} else if (!visitNumber.equals(other.visitNumber))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Website [id=" + id + ", visitDate=" + visitDate + ", siteUrl=" + siteUrl + ", visitNumber="
				+ visitNumber + "]";
	}

	
    public static Comparator<WebsiteVisit> sortByNumberOfVisitsAsc = new Comparator<WebsiteVisit>() {

	public int compare(WebsiteVisit s1, WebsiteVisit s2) {

	   return  (int) (s1.getVisitNumber() -s2.getVisitNumber());
	   
	   
   }};
   
   public static Comparator<WebsiteVisit> sortByNumberOfVisitsDesc = new Comparator<WebsiteVisit>() {

		public int compare(WebsiteVisit s1, WebsiteVisit s2) {

		   return  (int) (s2.getVisitNumber() -s1.getVisitNumber());
		   
		   
	   }};

	

	

}
