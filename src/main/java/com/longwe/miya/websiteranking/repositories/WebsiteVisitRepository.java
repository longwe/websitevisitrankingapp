package com.longwe.miya.websiteranking.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.longwe.miya.websiteranking.model.WebsiteVisit;

/**
 * Miya W. Longwe
 * The Interface WebsiteVisitRepository.
 */
@Repository
public interface WebsiteVisitRepository  extends JpaRepository<WebsiteVisit, Long>{
	
	
	/**
	 * Find website visi by site url starting with.
	 *
	 * @param siteUrl the site url
	 * @return the list
	 */
	List<WebsiteVisit> findWebsiteVisiBySiteUrlStartingWith(String siteUrl);
	
	/**
	 * Find website visit by visit date.
	 *
	 * @param visitDate the visit date
	 * @return the list
	 */
	List<WebsiteVisit>findWebsiteVisitByVisitDate(LocalDate visitDate);
	
}
