package com.longwe.miya.websiteranking.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.longwe.miya.websiteranking.model.WebsiteVisit;
import com.longwe.miya.websiteranking.repositories.WebsiteVisitRepository;

/**
 * The Class WebsiteVisitImpl.
 *
 * @author Miya W. Longwe
 */
@Service("websiteVisitService")
public class WebsiteVisitImpl implements WebsiteVisitService {
	
	/** The website visits repository. */
	@Autowired
	WebsiteVisitRepository  websiteVisitsRepository;

	/* (non-Javadoc)
	 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#findById(java.lang.Long)
	 */
	@Override
	public com.longwe.miya.websiteranking.model.WebsiteVisit findById(Long id) {
		return websiteVisitsRepository.findOne(id);
	}

	
	/* (non-Javadoc)
	 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#saveWebsiteVisit(com.longwe.miya.websiteranking.model.WebsiteVisit)
	 */
	@Override
	public void saveWebsiteVisit(WebsiteVisit websiteVisit) {
		
		websiteVisitsRepository.save(websiteVisit);
	}

	/* (non-Javadoc)
	 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#updateWebsiteVisit(com.longwe.miya.websiteranking.model.WebsiteVisit)
	 */
	@Override
	public void updateWebsiteVisit(WebsiteVisit websiteVisit) {
		websiteVisitsRepository.save(websiteVisit);
	}

	/* (non-Javadoc)
	 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#deleteWebsiteVisitById(java.lang.Long)
	 */
	@Override
	public void deleteWebsiteVisitById(Long id) {
		websiteVisitsRepository.delete(id);
		
	}

	/* (non-Javadoc)
	 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#deleteAllWebsiteVisit()
	 */
	@Override
	public void deleteAllWebsiteVisit() {
   websiteVisitsRepository.deleteAll();
		
	}

	/* (non-Javadoc)
	 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#findAllWebsiteVisits()
	 */
	@Override
	public List<WebsiteVisit> findAllWebsiteVisits() {

		return websiteVisitsRepository.findAll();
	}

	/* (non-Javadoc)
	 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#isWebsiteVisitExist(com.longwe.miya.websiteranking.model.WebsiteVisit)
	 */
	@Override
	public boolean isWebsiteVisitExist(WebsiteVisit websiteVisit) {
		return websiteVisitsRepository.findWebsiteVisiBySiteUrlStartingWith(websiteVisit.getSiteUrl())!=null;
	}

	/* (non-Javadoc)
	 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#findAllVisitDates()
	 */
	@Override
	public Set<LocalDate> findAllVisitDates() {

		return getDistinctDates(websiteVisitsRepository.findAll());
	}
	
	/**
	 * Gets the distinct dates.
	 *
	 * @param visits the visits
	 * @return the distinct dates
	 */
	private Set<LocalDate> getDistinctDates(List<WebsiteVisit> visits){
		
		Set<LocalDate>  visitDate= new HashSet<LocalDate>();
		
		if (visits!=null){
			for (Iterator<WebsiteVisit> iterator = visits.iterator(); iterator.hasNext();) {
				WebsiteVisit websiteVisit = iterator.next();
				visitDate.add(websiteVisit.getVisitDate());
				
			}
		}
		return visitDate;
		
	}

	

	/* (non-Javadoc)
	 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#getTopFiveVisitedSitesByDate(java.time.LocalDate)
	 */
	@Override
	public List<WebsiteVisit> getTopFiveVisitedSitesByDate(LocalDate visitDate) {
		return websiteVisitsRepository.findWebsiteVisitByVisitDate(visitDate);
	}
	
/**
 * Gets the distinct url name.
 *
 * @param visits the visits
 * @return the distinct url name
 */
private Set<String> getDistinctUrlName(List<WebsiteVisit> visits){
		
		Set<String>  websiteUrl= new HashSet<String>();
		
		if (visits!=null){
			for (Iterator<WebsiteVisit> iterator = visits.iterator(); iterator.hasNext();) {
				WebsiteVisit websiteVisit = iterator.next();
				websiteUrl.add(websiteVisit.getSiteUrl());
				
			}
		}
		return websiteUrl;
		
	}


/* (non-Javadoc)
 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#findAllSiteUrlNames()
 */
@Override
public Set<String> findAllSiteUrlNames() {

	return getDistinctUrlName(websiteVisitsRepository.findAll());
}


/* (non-Javadoc)
 * @see com.longwe.miya.websiteranking.service.WebsiteVisitService#findWebsiteVisitBySiteUrlStartingWith(java.lang.String)
 */
@Override
public List<WebsiteVisit> findWebsiteVisitBySiteUrlStartingWith(String siteUrl) {

	return websiteVisitsRepository.findWebsiteVisiBySiteUrlStartingWith( siteUrl );
}




}
