/**
 * 
 */
package com.longwe.miya.websiteranking.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import com.longwe.miya.websiteranking.model.WebsiteVisit;

/**
 * The Interface WebsiteVisitService.
 *
 * @author Miya W. Longwe
 */
public interface WebsiteVisitService {
	
	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the website visit
	 */
	WebsiteVisit findById(Long id);

	/**
	 * Find website visit by site url starting with.
	 *
	 * @param siteUrl the site url
	 * @return the list
	 */
	List<WebsiteVisit> findWebsiteVisitBySiteUrlStartingWith(String siteUrl);
	

	/**
	 * Save website visit.
	 *
	 * @param websiteVisit the website visit
	 */
	void saveWebsiteVisit(WebsiteVisit websiteVisit);

	/**
	 * Update website visit.
	 *
	 * @param websiteVisit the website visit
	 */
	void updateWebsiteVisit(WebsiteVisit websiteVisit);

	/**
	 * Delete website visit by id.
	 *
	 * @param id the id
	 */
	void deleteWebsiteVisitById(Long id);

	/**
	 * Delete all website visit.
	 */
	void deleteAllWebsiteVisit();

	/**
	 * Find all website visits.
	 *
	 * @return the list
	 */
	List<WebsiteVisit> findAllWebsiteVisits();

	/**
	 * Checks if is website visit exist.
	 *
	 * @param websiteVisit the website visit
	 * @return true, if is website visit exist
	 */
	boolean isWebsiteVisitExist(WebsiteVisit websiteVisit);
	
	/**
	 * Find all visit dates.
	 *
	 * @return the sets the
	 */
	Set<LocalDate>findAllVisitDates();
	
	/**
	 * Gets the top five visited sites by date.
	 *
	 * @param visitDate the visit date
	 * @return the top five visited sites by date
	 */
	List<WebsiteVisit> getTopFiveVisitedSitesByDate(LocalDate visitDate);
	
	/**
	 * Find all site url names.
	 *
	 * @return the sets the
	 */
	Set<String>findAllSiteUrlNames();

}
