/* Uncomment this this section to load data into hsqldb for local testing
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','www.bing.com',14065457);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','www.ebay.com.au',19831166);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','www.facebook.com',104346720);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','mail.live.com',21536612);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','www.wikipedia.org',13246531);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','www.ebay.com.au',23154653);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','au.yahoo.com',11492756);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','www.google.com',26165099);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','www.youtube.com',68487810);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','www.wikipedia.org',16550230);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','ninemsn.com.au',21734381);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','mail.live.com',24344783);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','www.ebay.com.au',22598506);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','mail.live.com',24272437);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','www.bing.com',16041776);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','ninemsn.com.au',24241574);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','www.facebook.com',118984483);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','ninemsn.com.au',24521168);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','www.facebook.com',123831275);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','www.bing.com',16595739);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','www.facebook.com',118506019);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','www.google.com.au',170020924);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','www.youtube.com',69327140);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','mail.live.com',24772355);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','ninemsn.com.au',24555033);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','www.google.com',28996455);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','www.bing.com',16618315);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','www.google.com.au',171842376);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','www.youtube.com',59811438);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','www.netbank.commbank.com.au',13316233);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','www.netbank.commbank.com.au',13072234);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','www.ebay.com.au',22785028);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','www.wikipedia.org',16519992);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','www.bom.gov.au',14369775);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-27','www.google.com',29422150);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-20','www.youtube.com',69064107);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-06','www.google.com.au',151749278);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','www.wikipedia.org',16015926);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','www.google.com',29203671);
insert into app_website(id,visit_date, site_url, visit_num ) values(null,'2016-01-13','www.google.com.au',172220397);
*/




/* Uncomment this this section to load data into mysqldl for prod testing
insert into app_website(id,visit_date, site_url, visit_num ) values(1,'2016-01-06','www.bing.com',14065457);
insert into app_website(id,visit_date, site_url, visit_num ) values(2,'2016-01-06','www.ebay.com.au',19831166);
insert into app_website(id,visit_date, site_url, visit_num ) values(3,'2016-01-06','www.facebook.com',104346720);
insert into app_website(id,visit_date, site_url, visit_num ) values(4,'2016-01-06','mail.live.com',21536612);
insert into app_website(id,visit_date, site_url, visit_num ) values(5,'2016-01-06','www.wikipedia.org',13246531);
insert into app_website(id,visit_date, site_url, visit_num ) values(6,'2016-01-27','www.ebay.com.au',23154653);
insert into app_website(id,visit_date, site_url, visit_num ) values(7,'2016-01-06','au.yahoo.com',11492756);
insert into app_website(id,visit_date, site_url, visit_num ) values(8,'2016-01-06','www.google.com',26165099);
insert into app_website(id,visit_date, site_url, visit_num ) values(9,'2016-01-13','www.youtube.com',68487810);
insert into app_website(id,visit_date, site_url, visit_num ) values(10,'2016-01-27','www.wikipedia.org',16550230);
insert into app_website(id,visit_date, site_url, visit_num ) values(11,'2016-01-06','ninemsn.com.au',21734381);
insert into app_website(id,visit_date, site_url, visit_num ) values(12,'2016-01-20','mail.live.com',24344783);
insert into app_website(id,visit_date, site_url, visit_num ) values(13,'2016-01-20','www.ebay.com.au',22598506);
insert into app_website(id,visit_date, site_url, visit_num ) values(14,'2016-01-27','mail.live.com',24272437);
insert into app_website(id,visit_date, site_url, visit_num ) values(15,'2016-01-27','www.bing.com',16041776);
insert into app_website(id,visit_date, site_url, visit_num ) values(16,'2016-01-20','ninemsn.com.au',24241574);
insert into app_website(id,visit_date, site_url, visit_num ) values(17,'2016-01-20','www.facebook.com',118984483);
insert into app_website(id,visit_date, site_url, visit_num ) values(18,'2016-01-27','ninemsn.com.au',24521168);
insert into app_website(id,visit_date, site_url, visit_num ) values(19,'2016-01-27','www.facebook.com',123831275);
insert into app_website(id,visit_date, site_url, visit_num ) values(20,'2016-01-20','www.bing.com',16595739);
insert into app_website(id,visit_date, site_url, visit_num ) values(21,'2016-01-13','www.facebook.com',118506019);
insert into app_website(id,visit_date, site_url, visit_num ) values(22,'2016-01-20','www.google.com.au',170020924);
insert into app_website(id,visit_date, site_url, visit_num ) values(23,'2016-01-27','www.youtube.com',69327140);
insert into app_website(id,visit_date, site_url, visit_num ) values(25,'2016-01-13','mail.live.com',24772355);
insert into app_website(id,visit_date, site_url, visit_num ) values(26,'2016-01-13','ninemsn.com.au',24555033);
insert into app_website(id,visit_date, site_url, visit_num ) values(27,'2016-01-20','www.google.com',28996455);
insert into app_website(id,visit_date, site_url, visit_num ) values(28,'2016-01-13','www.bing.com',16618315);
insert into app_website(id,visit_date, site_url, visit_num ) values(29,'2016-01-27','www.google.com.au',171842376);
insert into app_website(id,visit_date, site_url, visit_num ) values(30,'2016-01-06','www.youtube.com',59811438);
insert into app_website(id,visit_date, site_url, visit_num ) values(31,'2016-01-13','www.netbank.commbank.com.au',13316233);
insert into app_website(id,visit_date, site_url, visit_num ) values(32,'2016-01-20','www.netbank.commbank.com.au',13072234);
insert into app_website(id,visit_date, site_url, visit_num ) values(33,'2016-01-13','www.ebay.com.au',22785028);
insert into app_website(id,visit_date, site_url, visit_num ) values(34,'2016-01-20','www.wikipedia.org',16519992);
insert into app_website(id,visit_date, site_url, visit_num ) values(35,'2016-01-27','www.bom.gov.au',14369775);
insert into app_website(id,visit_date, site_url, visit_num ) values(36,'2016-01-27','www.google.com',29422150);
insert into app_website(id,visit_date, site_url, visit_num ) values(37,'2016-01-20','www.youtube.com',69064107);
insert into app_website(id,visit_date, site_url, visit_num ) values(38,'2016-01-06','www.google.com.au',151749278);
insert into app_website(id,visit_date, site_url, visit_num ) values(39,'2016-01-13','www.wikipedia.org',16015926);
insert into app_website(id,visit_date, site_url, visit_num ) values(40,'2016-01-13','www.google.com',29203671);
insert into app_website(id,visit_date, site_url, visit_num ) values(41,'2016-01-13','www.google.com.au',172220397);
*/
