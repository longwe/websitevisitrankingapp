'use strict';

angular.module('websiteVisitRanking').factory('WebsiteVisitService',
    ['$localStorage', '$http', '$q', 'urls',
        function ($localStorage, $http, $q, urls) {

            var factory = {
                loadAllWebsiteVisits: loadAllWebsiteVisits,
                getAllWebsiteVisits: getAllWebsiteVisits,
                getWebsiteVisit: getWebsiteVisit,
                createWebsiteVisit: createWebsiteVisit,
                updateWebsiteVisit: updateWebsiteVisit,
                removeWebsiteVisit: removeWebsiteVisit,
                loadAllWebsiteUrls:loadAllWebsiteUrls
            };

            return factory;

            function loadAllWebsiteVisits() {
                console.log('Fetching all websitevisits');
                var deferred = $q.defer();
                $http.get(urls.WEBSITE_VISIT_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all websitevisits');
                            $localStorage.websitevisits = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading websitevisits');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }
            
            
            function loadAllWebsiteUrls() {
                console.log('Fetching all website urls');
                var deferred = $q.defer();
                $http.get(urls.WEBSITE_URL_NAMES_SERVICE_API)
                    .then(
                        function (response) {
                            console.log('Fetched successfully all website urls names');
                            $localStorage.websiteUrlNames = response.data;
                            deferred.resolve(response);
                        },
                        function (errResponse) {
                            console.error('Error while loading webisteurl name');
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function getAllWebsiteVisits(){
                return $localStorage.websitevisits;
            }

            function getWebsiteVisit(id) {
                console.log('Fetching WebsiteVisit with id :'+id);
                var deferred = $q.defer();
                $http.get(urls.WEBSITE_VISIT_SERVICE_API + id)
                    .then(
                        function (response) {
                            console.log('Fetched successfully WebsiteVisit with id :'+id);
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while loading websitevisit with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function createWebsiteVisit(websitevisit) {
                console.log('Creating WebsiteVisit');
                var deferred = $q.defer();
                $http.post(urls.WEBSITE_VISIT_SERVICE_API, websitevisit)
                    .then(
                        function (response) {
                            loadAllWebsiteVisits();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                           console.error('Error while creating WebsiteVisit : '+errResponse.data.errorMessage);
                           deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function updateWebsiteVisit(websitevisit, id) {
                console.log('Updating WebsiteVisit with id '+id);
                var deferred = $q.defer();
                $http.put(urls.WEBSITE_VISIT_SERVICE_API + id, websitevisit)
                    .then(
                        function (response) {
                            loadAllWebsiteVisits();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while updating WebsiteVisit with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

            function removeWebsiteVisit(id) {
                console.log('Removing WebsiteVisit with id '+id);
                var deferred = $q.defer();
                $http.delete(urls.WEBSITE_VISIT_SERVICE_API + id)
                    .then(
                        function (response) {
                            loadAllWebsiteVisits();
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            console.error('Error while removing WebsiteVisit with id :'+id);
                            deferred.reject(errResponse);
                        }
                    );
                return deferred.promise;
            }

        }
    ]);