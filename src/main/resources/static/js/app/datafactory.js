angular.module('websiteVisitRanking')
    .factory('dataFactory', ['$http', function($http) {

    var urlBase = 'http://localhost:8080/WebsiteVisitsRankingApp';
    var dataFactory = {};

    dataFactory.getWebsiteVisitDates = function () {
        return $http.get(urlBase + '/api/websiteVisitDates/');
    };
    
    dataFactory.getAllWebsiteSiteUrls = function () {
        return $http.get(urlBase + '/api/websiteUrlNames/');
    };

    dataFactory.getWebsite = function (id) {
        return $http.get(urlBase + '/' + id);
    };

    dataFactory.insertWebsite = function (cust) {
        return $http.post(urlBase, cust);
    };

    dataFactory.updateWebsite = function (cust) {
        return $http.put(urlBase + '/' + cust.ID, cust)
    };

    dataFactory.deleteWebsite = function (id) {
        return $http.delete(urlBase + '/' + id);
    };

    dataFactory.getWebsiteByDate = function (id) {
        return $http.get(urlBase + '/api/site/' + id);
    };
    
    dataFactory.getSiteUrlsByName = function (urlName) {
        return $http.get(urlBase + '/api/websiteUrlNames/' + urlName);
    };

    
    dataFactory.getMostOrLeastVisitedSite = function (searchQry) {
        return $http.get(urlBase + '/api/websitevisitrank/' + searchQry);
    };
    
    dataFactory.getLeastVisitedSite = function () {
        return $http.get(urlBase + '/api/websiteUrlNames/');
    };
    

    return dataFactory;
}]);