<div class="generic-container">
	<div class="panel panel-default">
		<!-- Default panel contents -->
		<div class="panel-heading">
			<span class="lead">Top 5 Websites Ranking Application</span>
		</div>

		<div class="panel-body">
			<div ng-init="myVar = 'image/image001.png'">
				<img ng-src="{{myVar}}">
			</div>
			<div class="formcontainer">
				<div class="alert alert-success" role="alert"
					ng-if="ctrl.successMessage">{{status}}</div>
				<div class="alert alert-danger" role="alert"
					ng-if="ctrl.errorMessage">{{ctrl.errorMessage}}</div>


				<div ng-controller="siteVisitController">
					<form name="websiteVisit" ng-submit="getCustomerOrders(item)"
						novalidate>

						<div class="form-group">
						<label class="col-md-2 control-lable" for="visitDate">Search By Site Date</label>
							<select name="name" id="id" ng-model="myItem"
								ng-options="item as item for item in websites"
								class="form-control" ng-change="getWebsiteByDate()">
							</select>
						</div>
						<div class="form-group">
							<div>
								<div class="form-group">
								<label class="col-md-2 control-lable" for="siteUrl">Search By Site Url</label>
									<select name="url" id="url" ng-model="urlname"
										ng-options="item as item for item in urls"
										class="form-control" ng-change="getSiteUrlsByName()">
									</select>
								</div>
								</div>
								</div>
								<div class="form-group">
									<div>
										<table>
											<tr>
												<td align="right">Search:</td>
												<td><select ng-model="queryBy">
														<option value="1">Most Visited Website</option>
														<option value="2">Least Visited Website</option>
												</select></td>
												<td><button type="button"
														ng-click="getMostOrLeastVisitedSite()"
														class="btn btn-success custom-width">Submit</button></td>
											</tr>
										</table>
									</div>
								</div>

								<div class="panel panel-default">
									<!-- Default panel contents -->
									<div class="panel-heading">
										<span class="lead">List of Five Mostly Visited Websites
										</span>
									</div>
									<div class="panel-body">
										<div class="table-responsive">
											<table class="table table-hover">
												<thead>
													<tr>
														<th>ID</th>
														<th>Visit Date</th>
														<th>Site Url</th>
														<th>Number of Visits</th>

													</tr>
												</thead>
												<tbody>
													<tr ng-repeat="u in orders | filter:queryFilter">
														<td>{{u.id}}</td>
														<td>{{u.convertedVisitDate}}</td>
														<td><a ng-href="http://{{u.siteUrl}}">{{u.siteUrl}}</a></td>
														<td>{{u.visitNumber}}</td>
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								</div>
					</form>
				</div>

			</div>
		</div>
	</div>



</div>