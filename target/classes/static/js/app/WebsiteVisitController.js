'use strict';

angular.module('websiteVisitRanking').controller('WebsiteVisitController',
    ['WebsiteVisitService', '$scope',  function( WebsiteVisitService, $scope) {

        var self = this;
        self.websiteVisit = {};
        self.websiteVisits=[];

        self.submit = submit;
        self.getAllWebsiteVisits = getAllWebsiteVisits;
        self.createWebsiteVisit = createWebsiteVisit;
        self.updateWebsiteVisit = updateWebsiteVisit;
        self.removeWebsiteVisit = removeWebsiteVisit;
        self.editWebsiteVisit = editWebsiteVisit;
        self.reset = reset;

        self.successMessage = '';
        self.errorMessage = '';
        self.done = false;

        self.onlyIntegers = /^\d+$/;
        self.onlyNumbers = /^\d+([,.]\d+)?$/;

        function submit() {
            console.log('Submitting');
            if (self.websiteVisit.id === undefined || self.websiteVisit.id === null) {
                console.log('Saving New WebsiteVisit', self.websiteVisit);
                createWebsiteVisit(self.websiteVisit);
            } else {
                updateWebsiteVisit(self.websiteVisit, self.websiteVisit.id);
                console.log('WebsiteVisit updated with id ', self.websiteVisit.id);
            }
        }

        function createWebsiteVisit(websiteVisit) {
            console.log('About to create websiteVisit');
            WebsiteVisitService.createWebsiteVisit(websiteVisit)
                .then(
                    function (response) {
                        console.log('WebsiteVisit created successfully');
                        self.successMessage = 'WebsiteVisit created successfully';
                        self.errorMessage='';
                        self.done = true;
                        self.websiteVisit={};
                        $scope.myForm.$setPristine();
                    },
                    function (errResponse) {
                        console.error('Error while creating WebsiteVisit');
                        self.errorMessage = 'Error while creating WebsiteVisit: ' + errResponse.data.errorMessage;
                        self.successMessage='';
                    }
                );
        }


        function updateWebsiteVisit(websiteVisit, id){
            console.log('About to update websiteVisit');
            WebsiteVisitService.updateWebsiteVisit(websiteVisit, id)
                .then(
                    function (response){
                        console.log('WebsiteVisit updated successfully');
                        self.successMessage='WebsiteVisit updated successfully';
                        self.errorMessage='';
                        self.done = true;
                        $scope.myForm.$setPristine();
                    },
                    function(errResponse){
                        console.error('Error while updating WebsiteVisit');
                        self.errorMessage='Error while updating WebsiteVisit '+errResponse.data;
                        self.successMessage='';
                    }
                );
        }


        function removeWebsiteVisit(id){
            console.log('About to remove WebsiteVisit with id '+id);
            WebsiteVisitService.removeWebsiteVisit(id)
                .then(
                    function(){
                        console.log('WebsiteVisit '+id + ' removed successfully');
                    },
                    function(errResponse){
                        console.error('Error while removing websiteVisit '+id +', Error :'+errResponse.data);
                    }
                );
        }


        function getAllWebsiteVisits(){
            return WebsiteVisitService.getAllWebsiteVisits();
        }

        function editWebsiteVisit(id) {
            self.successMessage='';
            self.errorMessage='';
            WebsiteVisitService.getWebsiteVisit(id).then(
                function (websiteVisit) {
                    self.websiteVisit = websiteVisit;
                },
                function (errResponse) {
                    console.error('Error while removing websiteVisit ' + id + ', Error :' + errResponse.data);
                }
            );
        }
        function reset(){
            self.successMessage='';
            self.errorMessage='';
            self.websiteVisit={};
            $scope.myForm.$setPristine(); //reset Form
        }
    }


    ]);