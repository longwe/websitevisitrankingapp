var app = angular.module('websiteVisitRanking',['ui.router','ngStorage']);

app.constant('urls', {
    BASE: 'http://localhost:8080/WebsiteVisitsRankingApp',
    WEBSITE_VISIT_SERVICE_API : 'http://localhost:8080/WebsiteVisitsRankingApp/api/websitevisit/',
    WEBSITE_URL_NAMES_SERVICE_API : 'http://localhost:8080/WebsiteVisitsRankingApp/api/websiteUrlNames/'
});

app.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'partials/list',
                controller:'WebsiteVisitController',
                controllerAs:'ctrl',
                resolve: {
                    websitevisits: function ($q, WebsiteVisitService) {
                        console.log('Load all websitevisits');
                        var deferred = $q.defer();
                        WebsiteVisitService.loadAllWebsiteVisits().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    },
                   
                    websiteUrlNames: function ($q, WebsiteVisitService) {
                        console.log('Load all Site Url Names');
                        var deferred = $q.defer();
                        WebsiteVisitService.loadAllWebsiteUrls().then(deferred.resolve, deferred.resolve);
                        return deferred.promise;
                    }
                    
                    
                }
            });
        $urlRouterProvider.otherwise('/');
    }]);

