angular
		.module('websiteVisitRanking')
		.controller(
				'siteVisitController',
				[
						'$scope',
						'dataFactory','$window',
						function($scope, dataFactory,$window) {

							$scope.status;
							$scope.websites;
							$scope.orders;
							$scope.visitDate=$scope.foo;
							$scope.urls;

							
							getAllWebsiteSiteUrls();
							
							getWebsiteVisitDates();
							
							function getWebsiteVisitDates() {
								dataFactory
										.getWebsiteVisitDates()
										.then(
												function(response) {
													$scope.websites = response.data;
												},
												function(error) {
													$scope.status = 'Unable to load website data: '
															+ error.message;
												});
							}
							
							
						function getAllWebsiteSiteUrls() {
								dataFactory
										.getAllWebsiteSiteUrls()
										.then(
												function(response) {
													$scope.status = 'Retrieved site url names!';
													$scope.urls = response.data;
												},
												function(error) {
													$scope.status = 'Unable to load website data: '
															+ error.message;
												});
							}

							$scope.updateWebsite = function(id) {
								var cust;
								for (var i = 0; i < $scope.websites.length; i++) {
									var currCust = $scope.websites[i];
									if (currCust.ID === id) {
										cust = currCust;
										break;
									}
								}

								dataFactory
										.updateWebsite(cust)
										.then(
												function(response) {
													$scope.status = 'Updated Website! Refreshing website list.';
												},
												function(error) {
													$scope.status = 'Unable to update website: '
															+ error.message;
												});
							};

							$scope.insertWebsite = function() {
								
								var cust = {
									ID : 10,
									FirstName : 'JoJo',
									LastName : 'Pikidily'
								};
								dataFactory
										.insertWebsite(cust)
										.then(
												function(response) {
													$scope.status = 'Inserted Website! Refreshing website list.';
													$scope.websites.push(cust);
												},
												function(error) {
													$scope.status = 'Unable to insert website: '
															+ error.message;
												});
							};

							$scope.deleteWebsite = function(id) {
								dataFactory
										.deleteWebsite(id)
										.then(
												function(response) {
													$scope.status = 'Deleted Website! Refreshing website list.';
													for (var i = 0; i < $scope.websites.length; i++) {
														var cust = $scope.websites[i];
														if (cust.ID === id) {
															$scope.websites
																	.splice(i,
																			1);
															break;
														}
													}
													$scope.orders = null;
												},
												function(error) {
													$scope.status = 'Unable to delete website: '
															+ error.message;
												});
							};

							
							$scope.getWebsiteByDate = function() {
								var visitdate =$scope.myItem;
								if (visitdate){
								dataFactory
										.getWebsiteByDate(visitdate)
										.then(
												function(response) {
													$scope.status = 'Retrieved orders!';
													$scope.orders = response.data;
												},
												function(error) {
													$scope.status = 'Error retrieving websites! '
															+ error.message;
												});
							}};
							
							$scope.getSiteUrlsByName = function() {
								var urlName =$scope.urlname;
								if (urlName){
								dataFactory
										.getSiteUrlsByName(urlName)
										.then(
												function(response) {
													$scope.status = 'Retrieved webistes by url name!';
													$scope.orders = response.data;
												},
												function(error) {
													$scope.status = 'Error retrieving webistes by url name!'
															+ error.message;
												});
							}};
							
							
							$scope.getMostOrLeastVisitedSite = function() {
								var qry =$scope.queryBy;
								if (qry){
								dataFactory
										.getMostOrLeastVisitedSite(qry)
										.then(
												function(response) {
													$scope.status = 'Retrieved the website information!';
													$scope.orders = response.data;
												},
												function(error) {
													$scope.status = 'Error retrieving webistes by url name!'
															+ error.message;
												});
							}};
							
							
							
							
							
							$scope.getSiteUrls = function() {
							
								dataFactory
										.getSiteUrlsNames()
										.then(
												function(response) {
													$scope.status = 'Retrieved site url names!';
													$scope.urls = response.data;
												},
												function(error) {
													$scope.status = 'Error retrieving site names! '
															+ error.message;
												});
							};
							
							
							
						
							       
						} ]);